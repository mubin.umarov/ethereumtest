const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu} = electron;

let mainWindow;

app.on('ready', function(){
    mainWindow = new BrowserWindow({});
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname,'createAccount.html'),
        protocol:'file',
        slashes: true
    }));
    console.log('app on ready');

    //const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    //Menu.setApplicationMenu(mainMenu);
});


const mainMenuTemplate = [
    {},
    {
        label: 'File',
        submenu: [
            {
                label: 'Create new wallet...',
                click() {
                    var createWindow = new BrowserWindow({});
                    createWindow.loadURL(url.format({
                        pathname: path.join(__dirname,'createAccount.html'),
                        protocol:'file',
                        slashes: true
                    }));
                }

            },
            {
                label: 'Load...'
            }
        ]
    },
    {
        label: 'Send'
    }
]